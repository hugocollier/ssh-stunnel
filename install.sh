#!/bin/bash
# Original script by : Muhammad Hasan
# ==================================================

# initializing var
export DEBIAN_FRONTEND=noninteractive
MYIP=$(wget -qO- ipinfo.io/ip);
MYIP2="s/xxxxxxxxx/$MYIP/g";
NET=$(ip -o $ANU -4 route show to default | awk '{print $5}');
source /etc/os-release
ver=$VERSION_ID

#detail nama perusahaan
country=ID
state=Indonesia
locality=Kalimantan
organization=digitalocean
organizationalunit=hassan.community
commonname=digitalocean.com
email=community@digitalocean.com

# simple password minimal
wget -O /etc/pam.d/common-password "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/plugins/password"
chmod +x /etc/pam.d/common-password

# go to root
cd

# Edit file /etc/systemd/system/rc-local.service
cat > /etc/systemd/system/rc-local.service <<-END
[Unit]
Description=/etc/rc.local
ConditionPathExists=/etc/rc.local
[Service]
Type=forking
ExecStart=/etc/rc.local start
TimeoutSec=0
StandardOutput=tty
RemainAfterExit=yes
SysVStartPriority=99
[Install]
WantedBy=multi-user.target
END

# nano /etc/rc.local
cat > /etc/rc.local <<-END
#!/bin/sh -e
# rc.local
# By default this script does nothing.
exit 0
END

# Ubah izin akses
chmod +x /etc/rc.local

# enable rc local
systemctl enable rc-local
systemctl start rc-local.service

# disable ipv6
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
sed -i '$ i\echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6' /etc/rc.local

#update
apt update -y
apt upgrade -y
apt dist-upgrade -y
apt-get remove --purge ufw firewalld -y
apt-get remove --purge exim4 -y

#install jq
apt -y install jq

# install wget and curl
apt -y install wget curl


# set time GMT +7
ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

# set locale
sed -i 's/AcceptEnv/#AcceptEnv/g' /etc/ssh/sshd_config

# install
apt-get --reinstall --fix-missing install -y linux-headers-cloud-amd64 bzip2 gzip coreutils wget jq screen rsyslog iftop htop net-tools zip unzip wget net-tools curl nano sed screen gnupg gnupg1 bc apt-transport-https build-essential dirmngr libxml-parser-perl git lsof

cat> /root/.profile << END
# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n || true
clear
neofetch
END
chmod 644 /root/.profile


# install badvpn
cd
wget -O /usr/bin/badvpn-udpgw "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/plugins/badvpn-udpgw64"
chmod +x /usr/bin/badvpn-udpgw
sed -i '$ i\screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 500' /etc/rc.local
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 500


# setting port ssh
cd
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g'
# /etc/ssh/sshd_config
sed -i '/Port 22/a Port 81' /etc/ssh/sshd_config
sed -i 's/#Port  22/Port 22/g' /etc/ssh/sshd_config
sed -i 's/#Port 22/Port 22/g' /etc/ssh/sshd_config
/etc/init.d/ssh restart

echo "=== Install Dropbear ==="
# install dropbear
apt -y install dropbear
sed -i 's/NO_START=1/NO_START=0/g' /etc/default/dropbear
sed -i 's/DROPBEAR_PORT=22/DROPBEAR_PORT=143/g' /etc/default/dropbear
sed -i 's/DROPBEAR_EXTRA_ARGS=/DROPBEAR_EXTRA_ARGS="-p 109 -p 110"/g' /etc/default/dropbear
echo "/bin/false" >> /etc/shells
echo "/usr/sbin/nologin" >> /etc/shells
/etc/init.d/ssh restart
/etc/init.d/dropbear restart

# install stunnel
apt install stunnel4 -y
cat > /etc/stunnel/stunnel.conf <<-END
cert = /etc/stunnel/stunnel.pem
client = no
socket = a:SO_REUSEADDR=1
socket = l:TCP_NODELAY=1
socket = r:TCP_NODELAY=1

[sshd]
accept = 222
connect = 127.0.0.1:22

[dropbear]
accept = 777
connect = 127.0.0.1:109

END

# make a certificate
openssl genrsa -out key.pem 2048
openssl req -new -x509 -key key.pem -out cert.pem -days 1095 \
-subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"
cat key.pem cert.pem >> /etc/stunnel/stunnel.pem

# konfigurasi stunnel
sed -i 's/ENABLED=0/ENABLED=1/g' /etc/default/stunnel4
/etc/init.d/stunnel4 restart


# banner /etc/issue.net
wget -O /etc/issue.net "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/plugins/bannerssh.conf"
echo "Banner /etc/issue.net" >>/etc/ssh/sshd_config
sed -i 's@DROPBEAR_BANNER=""@DROPBEAR_BANNER="/etc/issue.net"@g' /etc/default/dropbear


# Download Commands
cd /usr/bin
cd /usr/local/sbin/
wget -O bench-network "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/bench-network"
wget -O connections "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/connections"
wget -O create "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/create"
wget -O create_trial "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/create_trial"
wget -O delete_expired "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/delete_expired"
wget -O edit_dropbear "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/edit_dropbear"
wget -O edit_openssh "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/edit_openssh"
wget -O edit_ports "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/edit_ports"
wget -O edit_stunnel4 "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/edit_stunnel4"
wget -O menu "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/menu"
wget -O options "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/options"
wget -O ram "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/ram"
wget -O reboot_sys "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/reboot_sys"
wget -O reboot_sys_auto "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/reboot_sys_auto"
wget -O renew_account "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/renew_account"
wget -O restart_services "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/restart_services"
wget -O set_multilogin_autokill "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/set_multilogin_autokill"
wget -O set_multilogin_autokill_lib "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/set_multilogin_autokill_lib"
wget -O show_ports "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/show_ports"
wget -O user_delete "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/user_delete"
wget -O user_list "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/user_list"
wget -O speedtest "https://gitlab.com/hugocollier/ssh-stunnel/-/raw/main/menu/speedtest"
chmod +x bench-network
chmod +x connections
chmod +x create
chmod +x create_trial
chmod +x delete_expired
chmod +x edit_dropbear
chmod +x edit_openssh
chmod +x edit_ports
chmod +x edit_stunnel4
chmod +x menu
chmod +x options
chmod +x ram
chmod +x reboot_sys
chmod +x reboot_sys_auto
chmod +x renew_account
chmod +x restart_services
chmod +x set_multilogin_autokill
chmod +x set_multilogin_autokill_lib
chmod +x show_ports
chmod +x user_delete
chmod +x user_list
chmod +x speedtest


apt autoclean -y
apt -y remove --purge unscd
apt autoremove -y
# finishing


/etc/init.d/ssh restart
/etc/init.d/dropbear restart
/etc/init.d/stunnel4 restart
screen -dmS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300 --max-clients 500
history -c
echo "unset HISTFILE" >> /etc/profile
